import React from "react"
import { Helmet } from 'react-helmet';
import Layout from '../components/layout'
import Card from '../components/card'

export default () => {
    return (
        <Layout>
            <Helmet>
                <title>Projects - John Sedlak</title>
            </Helmet>

            <div className="mt-3 mb-3">
                <h1>Projects</h1>

                <hr />

                <div className="row">
                    <div className="col-sm-12 col-md-6 mb-3">
                        <Card title="This Site">
                            <p>Built with gatsby &amp; react. Based on the blog tutorial, incorporating some bootstrap and fontawesome for flavor.</p>

                            <div className="text-right">
                                <div className="btn-group">
                                    <a className="btn btn-outline-primary" href="https://gitlab.com/jsedlak/jswebsite">
                                        <i className="fab fa-github"></i> Source Code
                                    </a>
                                </div>
                            </div>
                        </Card>
                    </div>

                    <div className="col-sm-12 col-md-6 mb-3">
                        <Card title="NJ TT Cup Website">
                            <p>A simple React website for delivering NJ TT Cup results.</p>
                            <div className="text-right">
                                <div className="btn-group">
                                    <a className="btn btn-outline-primary" href="https://gitlab.com/jsedlak/njttcup">
                                        <i className="fab fa-gitlab"></i> Source Code
                                    </a>
                                    <a className="btn btn-outline-primary" href="http://njttcup.com">
                                        <i className="fa fa-desktop"></i> Website
                                    </a>
                                </div>
                            </div>
                        </Card>
                    </div>

                    <div className="col-sm-12 col-md-6 mb-3">
                        <Card title="Repository Abstract Examples">
                            <p>A tutorial repository to provide examples of abstracting the repository and similar patterns.</p>
                            <div className="text-right">
                                <div className="btn-group">
                                    <a className="btn btn-outline-primary" href="https://github.com/jsedlak/repository-abstracts">
                                        <i className="fab fa-github"></i> Source Code
                                    </a>
                                </div>
                            </div>
                        </Card>
                    </div>
                </div>
                
            </div>
        </Layout>
    )
}