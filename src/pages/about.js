import React from "react"
import { Helmet } from 'react-helmet';
import Layout from '../components/layout'

import JohnMtb from '../images/john-mtb.jpg'

export default () => {
    return (
        <Layout>
            <Helmet>
                <title>About - John Sedlak</title>
            </Helmet>

            <div className="mt-3 mb-3">
                <h1>About John Sedlak</h1>

                <p>John is an accomplished software engineer, having applied his knowledge to projects of all sorts, ranging from LMS development, CPG sites, charity sites, and custom enterprise cloud applications. A former Microsoft MVP for XNA, John started out with VB5 before learning C# and .NET upon their launch, followed by Sitecore in 2006.</p>

                <p>In his personal life, John is a devoted cyclist, averaging around 400 hours a year of riding time with a combination of road racing, cyclocross racing, and mountain biking. His achievements include the New Jersey Cat 4 Crit Champion, NJ Cat 3 Cup Champion, 3rd place NJ Senior Men TT Cup and Team Director/Owner of Watts Up Cycling. John is also a NICA Level 1 coach for the CMS Trailblazers.</p>

                <div class="text-center">
                    <div class="btn-group">
                        <a class="btn btn-outline-primary btn-lg" href="https://gitlab.com/jsedlak">
                            <i className="fab fa-gitlab"></i> GitLab
                        </a>
                        <a class="btn btn-outline-primary btn-lg" href="https://github.com/jsedlak">
                            <i className="fab fa-github"></i> GitHub
                        </a>
                        <a class="btn btn-outline-primary btn-lg" href="https://www.road-results.com/racer/54040">@Road Results</a>
                    </div>
                </div>
            </div>

            <img src={JohnMtb} alt="John mountain biking" className="img-responsive" />
        </Layout>
    )
}