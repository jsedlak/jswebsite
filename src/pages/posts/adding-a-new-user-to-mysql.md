---
title: "Adding a New User to MySQL"
date: "2017-04-15"
---

Whenever you boot up a service like MySQL which has a <code>root</code> user, it's best practice to create users for your specific needs that are limited by security and scope. In the case of running a WordPress or similar MySQL based application, it's best to limit the user to a specific database.

We begin by loading the MySQL CLI.

```bash
mysql -u root -p
```

We then create the new user. If you haven't created the database yet, now would be an excellent time.

```sql
CREATE USER 'wordpress_admin'@'localhost' IDENTIFIED BY 'someCOMPLEXpassword';
```

Note that we want to limit the user's scope to just `localhost` such that even if we were to open the server's ports, the user would not be able to logon remotely.

We then `grant` privileges to the user to access our specific database.

```sql
GRANT ALL PRIVILEGES ON wordpress_database.* TO 'wordpress_admin'@'localhost';
```

If you ever need to remove the user, you may do so with the following `DROP` command.

```sql
DROP USER 'wordpress_admin'@'localhost';
```