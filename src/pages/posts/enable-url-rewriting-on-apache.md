---
title: "Enable URL Rewriting on Apache"
date: "2016-02-20"
---

After installing apache, you can enable mod_rewrite by running the following commands.

```bash
sudo a2enmod rewrite
sudo service apache2 restart
```

You will probably want to use the `.htaccess` file to configure your mod_rewrite. In this case, you should make sure to allow overrides by modifying your site’s configuration file. If you haven’t already, I suggest creating/copying the default config in `/etc/apache2/sites-enabled`. To enable the `.htaccess` file, add the following. You may want to change a few of the settings to fit your specific needs, the key here is the `AllowOverride` line, which allows the file to be used.

```
<directory var="" www="">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,deny
    allow from all
</directory>
```

Pay attention to the Directory element and the path that follows. For me, this path is different and for you it may be different as well. And as usual, restart the apache service.

```bash
sudo service apache2 restart
```