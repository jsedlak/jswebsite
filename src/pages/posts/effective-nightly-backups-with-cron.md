---
title: "Effective Nightly Backups with cron"
date: "2016-01-28"
---

*Cron is a way of scheduling tasks on linux based machines; most often used to run nightly, weekly or monthly batch processes to move data or restart services. Crontab is the tool on ubuntu to interact with the cron daemon and really simplifies the process. To start, we need to start editing our crontab file. If this is your first time using crontab, it should prompt you that it’s creating a new file and ask you for an editor. I typically use nano on Ubuntu and vi/vim on other distros. To do this, run the following command.*

```bash
user@host> crontab -e
```

The format of the tasks is as follows. Keep in mind that for any of the time properties, an asterisk (*) may be used to signify "all."

```bash
minute hour day_of_month month day_of_week command
```

Our command is pretty straight forward. We want to run a shell script and store the output in a log file (for debugging). The path is not specific to crontab or cron jobs in general and may be changed to suit your needs. I generally keep my work in `/var` to align with the general use of the server (`/var/www/*`)

```text
0 23 * * * /var/scripts/nightly.sh > /var/scripts/cron.log
```

In `/var`, create your scripts directory and start editing `nightly.sh`. We'll break the file into three sections: MySQL backups, File backups, and Service restarts.

```bash
#!/bin/sh
echo "Making backups of MySQL databases"
mysqldump -u USERNAME -pPASSWORD DATABASE_NAME | gzip > "/var/backups/SITE_NAME.sql.gz"

echo "Making file backups of the websites"
tar -czf /var/backups/SITE_NAME.files.tar.gz /var/www/SITE_NAME

echo "Restarting services"
sudo service MySQL restart
sudo service apache2 restart

echo "Finished!"
```

Keep in mind that the specifics of each line are relative to your setup. In my case, I keep all my backups in <code>/var</code> and all my sites organized by `SITE_NAME` in `/var/www`.

To test this script, all you need to do is manually run it.

```bash
user@host> ./nightly.sh
```

If all is well, you'll see the backups appear in `/var/backups`! Happy cronning!