import React from "react"
import { Link, graphql } from 'gatsby'
import { Helmet } from 'react-helmet';
import Layout from '../components/layout'

export default ({data}) => {
    return (
        <Layout>
            <Helmet>
                <title>John Sedlak</title>
            </Helmet>

            {data.allMarkdownRemark.edges.map(({ node }) => (
            <div key={node.id} className="post-excerpt">
                <h3>{node.frontmatter.title}{" "}</h3>
                <div className="post-meta">{node.frontmatter.date}</div>

                <p>{node.excerpt}</p>

                <div className="text-right">
                    <Link className="btn btn-secondary" to={node.fields.slug}>
                        Continue Reading <i className="fa fa-chevron-circle-right"></i>
                    </Link>
                </div>
            </div>
            ))}
        </Layout>
    )
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
          }
          excerpt(pruneLength: 512)
        }
      }
    }
  }
`