import React from 'react';
import { Link } from "gatsby"
import Logo from '../images/js-logo.png'

export default () => (
    <aside id="Sidebar">
        <Link to="/">
            <img src={Logo} alt="John Sedlak" style={{width:'100%', height: 'auto'}} />
        </Link>

        <hr/>

        <ul className="nav nav-pills flex-column">
            <li className="nav-item">
                <Link to="/" className="nav-link">Home</Link>
            </li>
            <li className="nav-item">
                <Link to="/about" className="nav-link">About</Link>
            </li>
            <li className="nav-item">
                <Link to="/projects" className="nav-link">Projects</Link>
            </li>
        </ul>
    </aside>
)