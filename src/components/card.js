import React from 'react'

export default class Card extends React.Component {
    render() {
        return (
            <div className="card h-100">
                <div className="card-body">
                    <h2 className="card-title">{this.props.title}</h2>
                    {this.props.children}
                </div>
            </div>
        )
    }
}