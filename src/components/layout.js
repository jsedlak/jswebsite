import React from 'react'
import Sidebar from './sidebar'

import './bootstrap.css'
// import './fontawesome.css'
import './layout.css'

export default ({children}) => (
    <div id="Page" className="container-fluid" style={{maxWidth: '1400px'}}>
        <div className="row mt-3">
            <div className="col col-xs-12 col-sm-3">
                <Sidebar />
            </div>
            <div className="col col-xs-12 col-sm-9">
                {children}
            </div>
        </div>

        <div className="p-3">
            <div className="text-right">
                <small>&copy;2018 John Sedlak. All Rights Reserved.</small>
            </div>
        </div>
    </div>
)