module.exports = {
    siteMetadata: {
        title: `John Sedlak`,
    },
    plugins: [
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `src`,
                path: `${__dirname}/src/`,
            },
        },
        {
            resolve: `gatsby-transformer-remark`,
            options: {
                plugins: [
                    {
                        resolve: `gatsby-remark-prismjs`,
                        options: {
                            classPrefix: "language-",
                            inlineCodeMarker: null,
                            aliases: {},
                            showLineNumbers: true,
                        },
                    },
                    {
                        resolve: `gatsby-remark-images`,
                        options: {
                            maxWidth: 1200,
                            withWebp: true
                        },
                    }
                ],
            },
        },
        {
            resolve: `gatsby-plugin-favicon`,
            options: {
                logo: "./src/images/js-logo.png"
            }
        },
        {
            resolve: `gatsby-plugin-canonical-urls`,
            options: {
              siteUrl: `https://www.johnsedlak.com`,
            },
          },
    ],
}